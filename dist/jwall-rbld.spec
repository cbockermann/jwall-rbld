Name:		jwall-rbld
Version:	0.5.1
Release:	0%{?dist}
Summary:	The jwall-rbld is a tiny dynamic DNS server intended to serve a custom, local real-time block list.
Group:		admin
License:	GPLv3
URL:		http://www.jwall.org/jwall-rbld
BuildRoot:	.build_tmp


Group: Applications/System


%description
Real-time block lists have become a popular technique to maintain a central and 
dynamic database of malicious IP addresses. Most prominent examples are given by
spamhaus.org or other services, which provide an updated blacklist of spammer IP 
addresses.

The jwall-rbld package provides a small DNS server, which maintains a dynamic 
in-memory database of IP addresses. These can be interactively changed, resulting
in an easy-to-use and flexible real-time block list.



%_signature gpg
%_gpg_path /home/chris/.gpg
%_gpg_name Christian Bockermann <chris@jwall.org>
%_gpgbin /usr/bin/gpg

%files -f ../tmp/rpmfiles.list
%defattr(-,root,root)
%doc


%changelog
