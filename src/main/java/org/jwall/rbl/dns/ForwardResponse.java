/**
 * 
 */
package org.jwall.rbl.dns;

/**
 * @author chris
 *
 */
public class ForwardResponse extends Response {

    final byte[] responseBytes;

    /**
     * @param q
     */
    public ForwardResponse(Query q, byte[] responseBytes) {
        super(q);
        this.responseBytes = responseBytes;
    }

    public byte[] toByteArray() {
        return responseBytes;
    }
}
