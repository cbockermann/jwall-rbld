/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *  
 * This file is part of the jwall-rbld program. jwall-rbld is an implementation 
 * of a simple DNS server for running a local, customized real time block-list. 
 * More information and documentation for the jwall-rbld can be found at
 * 
 *                    http://www.jwall.org/jwall-rbld
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this 
 * program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.rbl.dns;

import java.util.ArrayList;
import java.util.List;

/**
 * This class implements the basis for all DNS queries. It provides a
 * parsing-method for reading query objects from a byte array.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class Query {

    int id = 0x0;
    int flags = 0;
    int qcount = 0;
    int acount = 0;
    int arCount = 0;
    int nsCount = 0;
    ArrayList<QuerySection> sections = new ArrayList<QuerySection>();
    byte[] bytes = null;

    private Query(int id) {
        this();
        this.id = id;
    }

    private Query() {
        this.sections = new ArrayList<QuerySection>();
    }

    public int getId() {
        return id;
    }

    public Integer getFlags() {
        return flags;
    }

    public Integer getARCount() {
        return arCount;
    }

    public Integer getNSCount() {
        return nsCount;
    }

    public int getNumberOfSections() {
        return sections.size();
    }

    public Integer getQCount() {
        return qcount;
    }

    public Integer getACount() {
        return acount;
    }

    public List<QuerySection> getSections() {
        return new ArrayList<QuerySection>(this.sections);
    }

    public static Query parse(byte[] buf, int offset) throws Exception {
        Query q = new Query();

        int ptr = offset;
        int firstByte = (0x000000FF & ((int) buf[ptr++]));
        int secondByte = (0x000000FF & ((int) buf[ptr++]));
        char id = (char) (firstByte << 8 | secondByte);

        int qid = (int) id;
        q.id = qid;

        q.flags = 0 + (buf[ptr++] << 8) + buf[ptr++];
        q.qcount = 0 + (buf[ptr++] << 8) + buf[ptr++];
        q.acount = 0 + (buf[ptr++] << 8) + buf[ptr++];
        q.nsCount = 0 + (buf[ptr++] << 8) + buf[ptr++];

        for (int i = 0; i < q.qcount; i++) {
            final QuerySection qsect = QuerySection.read(buf, offset + 12);
            q.sections.add(qsect);
        }

        q.bytes = new byte[buf.length - offset];
        System.arraycopy(buf, offset, q.bytes, 0, buf.length - offset);

        return q;
    }
}
