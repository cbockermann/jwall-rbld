/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *  
 * This file is part of the jwall-rbld program. jwall-rbld is an implementation 
 * of a simple DNS server for running a local, customized real time block-list. 
 * More information and documentation for the jwall-rbld can be found at
 * 
 *                    http://www.jwall.org/jwall-rbld
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this 
 * program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.rbl.dns;

import java.nio.ByteBuffer;
import java.util.ArrayList;

public class DNSParser {

	public static String[] readDNSstring( ByteBuffer buf ){
		return readDNSstring( buf.array() );
	}
	
	public static String[] readDNSstring( byte[] buf ){
		
		ArrayList<String> parts = new ArrayList<String>();
		int ptr = 0;
		byte len = buf[ptr++];
		while( len > 0 ){
			
			StringBuffer s = new StringBuffer();
			for( byte i = 0; i < len; i++ )
				s.append( (char) buf[ptr++] );
			
			parts.add( s.toString() );
			len = buf[ptr++];
		}

		String[] result = new String[ parts.size() ];
		parts.toArray( result );
		return result;
	}

	public static String printDNSstring( String[] str ){
		StringBuffer s = new StringBuffer();
		for( int i = 0; i < str.length; i++ ){
			s.append( str[i] );
			if( i+1 < str.length )
				s.append( "." );
		}
		return s.toString();
	}

	public static ByteBuffer toDNSdata( String str ){
		return toDNSdata( str.split( "\\." ) );
	}
	
	public static byte[] toByteArray( String str ){
		return toByteArray( str.split( "\\." ) );
	}
	
	public static byte[] toByteArray( String[] str ){
		int size = 1;
		for( String s : str )
			size += s.length();
		
		byte[] buf = new byte[size + str.length];
		int ptr = 0;
		for( String s : str ){
			buf[ptr++] = (byte) s.length();
			for( int i = 0; i < s.length(); i++ )
				buf[ptr++] = ( (byte) s.charAt( i ) );
		}
		buf[ptr] = 0;  // the final "closing" byte
		return buf;
	}

	
	
	public static ByteBuffer toDNSdata( String[] str ){
		int size = 1;
		for( String s : str )
			size += s.length();
		
		ByteBuffer buf = ByteBuffer.allocate( size );
		for( String s : str ){
			buf.put( (byte) s.length() );
			for( int i = 0; i < s.length(); i++ )
				buf.put( (byte) s.charAt( i ) );
		}
		buf.put( (byte) 0 );
		buf.flip();
		return buf;
	}
}
