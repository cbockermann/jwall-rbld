/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *  
 * This file is part of the jwall-rbld program. jwall-rbld is an implementation 
 * of a simple DNS server for running a local, customized real time block-list. 
 * More information and documentation for the jwall-rbld can be found at
 * 
 *                    http://www.jwall.org/jwall-rbld
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this 
 * program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.rbl.dns;

/**
 * This class represents a simple QuerySection.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public final class QuerySection {
    // the IN query class
    public final static int QUERY_CLASS_IN = 0x01;

    // the query type A
    public final static int QUERY_TYPE_A = 0x01;

    // the query type NS
    public final static int QUERY_TYPE_NS = 0x02;

    // the query type CNAME
    public final static int QUERY_TYPE_CNAME = 0x05;

    // the query type SOA
    public final static int QUERY_TYPE_SOA = 0x06;

    // the query type PTR
    public final static int QUERY_TYPE_PTR = 0x0C;

    // the query type MX
    public final static int QUERY_TYPE_MX = 0x0F;

    // the query type TXT
    public final static int QUERY_TYPE_TXT = 0x10;

    // this section's query class
    int qc = QUERY_CLASS_IN;

    // this section's query type
    int qt = QUERY_TYPE_A;

    // this section's query name (i.e. query value)
    byte[] qname = new byte[0];

    /**
     * Query sections are usually simply parsed by a static parse-method
     * provided by this class.
     */
    private QuerySection() {
    }

    /**
     * Returns the query class of this section.
     * 
     * @return
     */
    public int getQClass() {
        return qc;
    }

    /**
     * Reqturns the query type of this section.
     * 
     * @return
     */
    public int getQType() {
        return qt;
    }

    /**
     * This method returns the query name as a readable string.
     * 
     * @return
     */
    public String getQName() {
        StringBuffer s = new StringBuffer();
        String[] vals = DNSParser.readDNSstring(qname);
        for (int i = 0; i < vals.length; i++) {
            s.append(vals[i]);
            if (i + 1 < vals.length)
                s.append(".");
        }
        return s.toString();
    }

    /**
     * This method returns the complete length in bytes of this query section.
     * 
     * @return
     */
    public int length() {
        return qname.length + 2 + 2;
    }

    /**
     * This method parses a query section from the given buffer. Parsing starts
     * at the specified offset within that buffer.
     * 
     * @param buf
     * @param offset
     * @return
     * @throws Exception
     */
    public static QuerySection read(byte[] buf, int offset) throws Exception {

        QuerySection sect = new QuerySection();

        int ptr = offset;
        byte len = buf[ptr++];

        while (len > 0) {
            ptr += len;
            len = buf[ptr++];
        }

        sect.qname = new byte[ptr - offset];
        for (int i = offset; i < ptr; i++)
            sect.qname[i - offset] = buf[i];

        int firstByte = (0x000000FF & ((int) buf[ptr++]));
        int secondByte = (0x000000FF & ((int) buf[ptr++]));
        sect.qt = (firstByte << 8 | secondByte);

        firstByte = (0x000000FF & ((int) buf[ptr++]));
        secondByte = (0x000000FF & ((int) buf[ptr++]));
        sect.qc = (firstByte << 8 | secondByte);
        return sect;
    }

    /**
     * This method returns this query section in form of a byte array, which
     * reflects the appropriate format for sending this section over the wire.
     * 
     * @return
     */
    public byte[] toByteArray() {
        byte[] data = new byte[length()];

        int ptr = 0;
        for (int i = 0; i < qname.length; i++)
            data[ptr++] = qname[i];

        data[ptr++] = ((byte) ((qt & 0xff00) >> 8));
        data[ptr++] = ((byte) (qt & 0x00ff));
        data[ptr++] = ((byte) ((qc & 0xff00) >> 8));
        data[ptr++] = ((byte) (qc & 0x00ff));
        return data;
    }

    public String toString() {
        return "{ class: '" + this.getQClass() + " type '" + this.getQType() + "', value: '" + this.getQName() + "' }";
    }
}
