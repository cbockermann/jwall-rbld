/**
 * 
 */
package org.jwall.rbl.dns;

/**
 * @author chris
 *
 */
public class Message {

    public final static int RESPONSE_FLAG = 1 << 15;
    public final static int OP_QUERY = 0;
    public final static int OP_IQUERY = 1;
    public final static int OP_STATUS = 2;
    public final static int OP_RESERVED = 3;
    public final static int OP_NOTIFY = 4;
    public final static int OP_UPDATE = 5;

    public final static int AUTHORITATIVE_FLAG = 1 << 10;
    public final static int TRUNCATION_FLAG = 1 << 9;
    public final static int RD_FLAG = 1 << 8;
    public final static int RA_FLAG = 1 << 7;

    public final static int RC_NO_ERROR = 0;
    public final static int RC_FORMAT_ERROR = 1;
    public final static int RC_SERVER_FAILURE = 2;
    public final static int RC_NAME_ERROR = 0x3;
    public final static int RC_NOT_IMPLEMENTED = 4;
    public final static int RC_REFUSED = 5;
    public final static int RC_XY_DOMAIN = 6;
    public final static int RC_YX_RR_SET = 7;
    public final static int RC_NX_RR_SET = 8;
    public final static int RC_NOT_AUTH = 9;
    public final static int RC_NOT_ZONE = 10;

    final int id;
    final int flags;
    final int qcount;
    final int acount;
    final int nsCount;

    public Message(byte[] buf, int offset, int lengt) {

        int ptr = offset;
        int firstByte = (0x000000FF & ((int) buf[ptr++]));
        int secondByte = (0x000000FF & ((int) buf[ptr++]));
        char id = (char) (firstByte << 8 | secondByte);

        this.id = (int) id;
        flags = 0 + (buf[ptr++] << 8) + buf[ptr++];
        qcount = 0 + (buf[ptr++] << 8) + buf[ptr++];
        acount = 0 + (buf[ptr++] << 8) + buf[ptr++];
        nsCount = 0 + (buf[ptr++] << 8) + buf[ptr++];
    }

    public String toString() {

        String flagString = "[";

        if ((flags & RESPONSE_FLAG) > 0) {
            flagString += "R";
        } else {
            flagString += "Q";
        }

        if ((flags & AUTHORITATIVE_FLAG) > 0) {
            flagString += "/AA";
        } else {
            flagString += "/";
        }

        if ((flags & TRUNCATION_FLAG) > 0) {
            flagString += "/TC";
        } else {
            flagString += "/";
        }

        if ((flags & RD_FLAG) > 0) {
            flagString += "/RD";
        } else {
            flagString += "/";
        }

        if ((flags & RA_FLAG) > 0) {
            flagString += "/RA";
        } else {
            flagString += "/";
        }
        flagString += "]";

        return "{ id: " + id + ", flags: " + flagString + ", qcount: " + qcount + ", acount:" + acount + ", nsCount: "
                + nsCount + "}";
    }
}
