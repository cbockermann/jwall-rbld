/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *  
 * This file is part of the jwall-rbld program. jwall-rbld is an implementation 
 * of a simple DNS server for running a local, customized real time block-list. 
 * More information and documentation for the jwall-rbld can be found at
 * 
 *                    http://www.jwall.org/jwall-rbld
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this 
 * program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.rbl.dns;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import org.jwall.util.PropertyReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * This class implements an IP-based permission scheme. Permissions are read
 * from a file in format
 * </p>
 * 
 * <pre>
 * addr = permission
 * </pre>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class RblSecurityManager {

	/* The logger for this class */
	static Logger log = LoggerFactory.getLogger(RblSecurityManager.class);

	/** This class is a singleton. This is the only global instance. */
	private final static RblSecurityManager globalInstance = new RblSecurityManager();

	/** The string value of permission 'block' */
	public final static String BLOCK_PERMISSION = "block";

	/** The string value of permission 'unblock' */
	public final static String UNBLOCK_PERMISSION = "unblock";

	/** The string value representing all permissions */
	public final static String ALL_PERMISSION = "*";

	/** This is the list of all available (assignable) permissions */
	public final static String[] PERMISSIONS = { BLOCK_PERMISSION,
			UNBLOCK_PERMISSION, };

	/** This map provides a mapping of addresses to permissions. */
	Map<String, Set<String>> permissions = new LinkedHashMap<String, Set<String>>();

	/**
	 * Creates an instance of the security manager which reads permissions from
	 * the given file.
	 * 
	 * @param file
	 * @throws Exception
	 */
	private RblSecurityManager() {
		log.debug("Initializing SecurityManager");
	}

	/**
	 * This method returns the global RblSecurityManager instance.
	 * 
	 * @return The global RblSecurityManager.
	 */
	public static RblSecurityManager getInstance() {
		return globalInstance;
	}

	public void readPermissions(File file) throws Exception {
		log.info("Reading permissions from file {}", file.getAbsolutePath());

		if (!file.canRead())
			throw new Exception("Cannot open file '" + file.getAbsolutePath()
					+ "' for reading!");

		readPermissions(new FileInputStream(file));
	}

	/**
	 * Parse the given file and store all permissions in the global hash.
	 * 
	 * @param file
	 * @throws Exception
	 */
	public void readPermissions(InputStream inputStream) throws Exception {

		permissions = new LinkedHashMap<String, Set<String>>();

		Properties p = PropertyReader.read(inputStream);

		//
		// iterate over the (key,value) pairs
		//
		for (Object k : p.keySet()) {
			String key = k.toString();
			if ("".equals(key.trim())) {
				log.info("Skipping key '{}'", key);
				continue;
			}

			log.info("Parsing address from '{}'", key);
			InetAddress addr = InetAddress.getByName(key);
			log.info("Address is: {} (toString: {})", addr.getHostAddress(),
					addr.toString());
			String[] ps = p.getProperty(key).split(",");

			Set<String> perms = new TreeSet<String>();
			for (String perm : ps) {
				if ("*".equals(perm)) {
					for (String permission : PERMISSIONS)
						perms.add(permission);
				} else {
					if (isPermission(perm.toLowerCase()))
						perms.add(perm.toLowerCase());
				}
			}

			log.debug("   Adding permissions {} for address {}", perms,
					addr.getHostAddress());
			permissions.put(addr.getHostAddress(), perms);
		}
	}

	/**
	 * <p>
	 * Checks whether the given address <code>addr</code> has the specified
	 * permission.
	 * </p>
	 * 
	 * @param addr
	 *            The address to check.
	 * @param perm
	 *            The permission which is queried for this address.
	 * @return
	 */
	public boolean hasPermission(InetAddress addr, String perm) {
		log.debug("Checking permission {} for address {}", perm,
				addr.getHostAddress());
		Set<String> perms = permissions.get(addr.getHostAddress());
		log.debug("   permissions stored for {} are: {}",
				addr.getHostAddress(), perms);

		if (perms == null)
			return false;

		return perms.contains(perm);
	}

	/**
	 * Checks whether the given string corresponds to a permission value.
	 * 
	 * @param perm
	 *            The string to check
	 * @return <code>true</code>, if the string corresponds to a permission,
	 *         else <code>false</code>.
	 */
	private boolean isPermission(String perm) {
		for (String p : PERMISSIONS)
			if (p.equalsIgnoreCase(perm))
				return true;

		return false;
	}
}