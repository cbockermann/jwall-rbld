/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *  
 * This file is part of the jwall-rbld program. jwall-rbld is an implementation 
 * of a simple DNS server for running a local, customized real time block-list. 
 * More information and documentation for the jwall-rbld can be found at
 * 
 *                    http://www.jwall.org/jwall-rbld
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this 
 * program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.rbl.dns;

import java.nio.ByteBuffer;

public class ResourceRecord {
	public final static int TYPE_A  = 0x1;
	public final static byte[] TYPE_NS = new byte[]{ 0x0, 0x1 };
	public final static byte TYPE_CNAME = 5;
	public final static byte TYPE_SOA = 6;
	public final static byte TYPE_PTR = 12;
	public final static byte TYPE_MX = 15;
	public final static byte TYPE_TXT = 16;
	
	public final static int RCLASS_IN = 0x1;
	
	byte[] value;
	int rtype  = TYPE_A;
	int rclass = RCLASS_IN;
	int ttl = 0;
	byte[] rdata = new byte[0];
	
	protected ResourceRecord( String value, int type, int ttl ){
		this.value = DNSParser.toByteArray( value );
		this.rtype = type;
		this.ttl = ttl;
	}
	
	public int length(){
		return value.length + 2 + 2 + 4 + 2 + rdata.length;
	}
	
	public byte[] toByteArray(){
		byte[] data = new byte[ length() ];
		ByteBuffer buf = ByteBuffer.wrap( data ); //rtype.length + rclass.length + 4 + rdlength.length + rdata.length + data.limit() );
		buf.put( value );

		
		// write out the record type and record classes as 2-byte integers 
		//
		buf.put( (byte) ( ( rtype & 0xff00 ) >> 8 ) );
		buf.put( (byte) (   rtype & 0x00ff ) );

		buf.put( (byte) ( ( rclass & 0xff00 ) >> 8 ) );
		buf.put( (byte) (   rclass & 0x00ff ) );
		
		// write out the time-to-live
		//
		buf.putInt( ttl );

		int rdlength = rdata.length;
		buf.put( (byte) ( ( rdlength & 0xff00     ) >>  8 ) );
		buf.put( (byte) (   rdlength & 0x00ff ) );
		
		buf.put( rdata );
		return data;
	}
}
