/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *  
 * This file is part of the jwall-rbld program. jwall-rbld is an implementation 
 * of a simple DNS server for running a local, customized real time block-list. 
 * More information and documentation for the jwall-rbld can be found at
 * 
 *                    http://www.jwall.org/jwall-rbld
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this 
 * program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.rbl.dns;

import java.nio.ByteBuffer;
import java.util.ArrayList;

public class Response {

	public final static int RESPONSE_FLAG = 1 << 15;
	public final static int OP_QUERY = 0;
	public final static int OP_IQUERY = 1;
	public final static int OP_STATUS = 2;
	public final static int OP_RESERVED = 3;
	public final static int OP_NOTIFY = 4;
	public final static int OP_UPDATE = 5;
	
	public final static int AUTHORITATIVE_FLAG = 1 << 10;
	public final static int TRUNCATION_FLAG = 1 << 9;
	public final static int RD_FLAG = 1 << 8;
	public final static int RA_FLAG = 1 << 7;
	
	public final static int RC_NO_ERROR = 0;
	public final static int RC_FORMAT_ERROR = 1;
	public final static int RC_SERVER_FAILURE = 2;
	public final static int RC_NAME_ERROR = 0x3;
	public final static int RC_NOT_IMPLEMENTED = 4;
	public final static int RC_REFUSED = 5;
	public final static int RC_XY_DOMAIN = 6;
	public final static int RC_YX_RR_SET = 7;
	public final static int RC_NX_RR_SET = 8;
	public final static int RC_NOT_AUTH = 9;
	public final static int RC_NOT_ZONE = 10;
	
	int id;
	int arCount = 0;
	int nsCount = 0;
	int status = RC_NO_ERROR;
	ArrayList<QuerySection> queries = new ArrayList<QuerySection>();
	ArrayList<ResourceRecord> answers = new ArrayList<ResourceRecord>();
	
	public Response( Query q ){
		this.id = q.getId();
		queries.addAll( q.getSections() );
	}
	
	public void add( ResourceRecord rr ){
		answers.add( rr );
	}
	
	public int getNumberOfRecords(){
		return answers.size();
	}
	
	
	public void setStatus( int status ){
		this.status = status;
	}
	
	
	public byte[] toByteArray(){

		int i = 0;
		int len = 12;
		ByteBuffer[] qs = new ByteBuffer[ queries.size() ];
		for( QuerySection q : queries ){
			byte[] b = q.toByteArray();
			qs[i++] = ByteBuffer.wrap( b );
			len += b.length;
		}
		
		i=0;
		ByteBuffer[] rrs = new ByteBuffer[ answers.size() ];
		for( ResourceRecord rr : answers ){
			byte[] b = rr.toByteArray();
			rrs[i++] = ByteBuffer.wrap( b );
			len += b.length;
		}
		
		byte[] data = new byte[len];
		
		
		// put ID into response
		//
		ByteBuffer buf = ByteBuffer.wrap( data );
		
		buf.put( (byte) ( ( id & 0xff00 ) >> 8 ) );
		buf.put( (byte) (   id & 0xff   ) );

		// set flags
		//
		int flags = RESPONSE_FLAG | AUTHORITATIVE_FLAG;
		buf.put( (byte) ( ( flags & 0xff00 ) >> 8 ) );
		buf.put( (byte) ( status & 0x07 ) );
		
		// question count
		buf.put( (byte) ( ( queries.size() & 0xff00 ) >> 8 ) );
		buf.put( (byte) (   queries.size() & 0xff ) );
		
		// answer count
		buf.put( (byte) ( ( answers.size() & 0xff00 ) >> 8 ) );
		buf.put( (byte) (   answers.size() & 0xff ) );
		
		// ns count
		buf.put( (byte) ( ( nsCount & 0xff00 ) >> 8 ) );
		buf.put( (byte) (   nsCount & 0xff          ) );
		
		// ar count
		buf.put( (byte) ( ( arCount & 0xff00 ) >> 8 ) );
		buf.put( (byte) (   arCount & 0xff ) );
		
		for( ByteBuffer q : qs )
			buf.put( q );
		
		for( ByteBuffer rr : rrs )
			buf.put( rr );
		
		return data;
	}
}
