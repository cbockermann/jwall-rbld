/**
 * 
 */
package org.jwall.rbl.dns;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.jwall.rbl.RblServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chris
 *
 */
public class ForwardHandler {

    static Logger log = LoggerFactory.getLogger(ForwardHandler.class);

    final RblServer server;

    final InetAddress forwardServer;
    final Integer forwardPort;
    final List<InetAddress> servers = new ArrayList<InetAddress>();

    Map<Integer, Query> queries = new LinkedHashMap<Integer, Query>();

    public ForwardHandler(RblServer server) throws Exception {
        this.server = server;

        String fwdAddress = server.properties().getProperty("forward", "8.8.8.8");
        forwardServer = InetAddress.getByName(fwdAddress);

        forwardPort = new Integer(server.properties().getProperty("forward.port", "53"));
        log.info("Forwarding non-rbl queries to {}:{}", forwardServer.getHostAddress(), forwardPort);
    }

    public synchronized Response process(InetAddress source, Query query) {
        log.debug("Forwarding query from '{}': {}", source.getHostAddress(), query.getSections());

        try {
            DatagramSocket sender = new DatagramSocket();

            byte[] buf = query.bytes;
            DatagramPacket packet = new DatagramPacket(buf, 0, buf.length);
            packet.setAddress(forwardServer);
            packet.setPort(forwardPort);

            sender.send(packet);

            byte[] buffer = new byte[1024];
            DatagramPacket response = new DatagramPacket(buffer, 0, buffer.length);
            sender.receive(response);

            int len = response.getLength();

            // if (log.isDebugEnabled()) {
            Message msg = new Message(buffer, response.getOffset(), response.getLength());
            log.info("Received response from {}: {}", response.getAddress().getHostAddress(), msg);
            // }

            byte[] output = new byte[len];
            System.arraycopy(buffer, response.getOffset(), output, 0, len);

            sender.close();
            return new ForwardResponse(query, output);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}