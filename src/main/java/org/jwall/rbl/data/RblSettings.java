package org.jwall.rbl.data;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.jwall.rbl.RblServer;

/**
 * <p>
 * This class provides a container for basic settings for the RblServer. The
 * class basically is just Java Properties class extended by a set of default
 * settings.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class RblSettings extends Properties {

	/** The unique class ID */
	private static final long serialVersionUID = 6417122703384246759L;

	/**
	 * Creates a new set of default settings.
	 */
	public RblSettings() {
		setProperty("rbl.domain", "rbl.localnet");
		setProperty("rbl.port", "15353");
		setProperty("rbl.address", "127.0.0.1");
		setProperty("rbl.admin.port", "15354");
		setProperty("rbl.file", "/var/lib/jwall-rbld/local.rbl");
		setProperty("rbl.permission.file", "/etc/jwall-rbld.permissions");
	}

	/**
	 * This static method returns the set of default properties for any
	 * RblServer.
	 * 
	 * @return
	 */
	public static RblSettings getDefaults() {
		return new RblSettings();
	}

	/**
	 * <p>
	 * This method reads properties from the given file. It will determine the
	 * default settings by the method <code>getDefaults()</code> and override
	 * all properties found in the given file.
	 * </p>
	 * <p>
	 * In a addition to that, the system properties are checked as well. Any
	 * system property, e.g. set using
	 * </p>
	 * 
	 * <pre>
	 *    java ... -Drbl.domain=rbl.localnet  ...
	 * </pre>
	 * <p>
	 * will override properties found in the defaults or in the specified
	 * settings file.
	 * </p>
	 * 
	 * @param file
	 * @return
	 */
	public static RblSettings read(String file) throws Exception {

		// start off with the defaults
		//
		RblSettings p = new RblSettings();

		//
		// if the given configuration file exists we try to
		// parse it and add the settings of that file
		//
		File config = new File(file);
		//RblServer.log
				//.info("Reading settings from {}", config.getAbsolutePath());
		if (config.isFile() && config.canRead()) {
			try {
				Properties cfg = new Properties();
				cfg.load(new FileInputStream(config));
				for (Object k : cfg.keySet())
					p.setProperty(k.toString(), cfg.getProperty(k.toString()));
			} catch (Exception e) {
				throw new Exception("Failed to read settings from '"
						+ config.getAbsolutePath() + "': " + e.getMessage());
			}
		}

		//
		// override any properties which have been set using the system
		// properties
		//
		for (String key : RblServer.PROPERTY_NAMES) {
			if (System.getProperty(key) != null
					&& !"".equals(System.getProperty(key).trim())) {
				p.setProperty(key, System.getProperty(key));
			}
		}

		return p;
	}
}
