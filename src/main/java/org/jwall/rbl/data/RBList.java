/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *  
 * This file is part of the jwall-rbld program. jwall-rbld is an implementation 
 * of a simple DNS server for running a local, customized real time block-list. 
 * More information and documentation for the jwall-rbld can be found at
 * 
 *                    http://www.jwall.org/jwall-rbld
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this 
 * program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.rbl.data;

import java.util.List;



/**
 * <p>
 * This interface defines an abstract block list. Elements added to this
 * list will be served (i.e. may result in blocking).
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface RBList {
	
	/**
	 * Returns the entry for the given name.
	 * 
	 * @param name
	 * @return
	 */
	public RBListEntry lookup( String name );
	
	
	/**
	 * Checks whether the given key (e.g. IP address, hostname) is
	 * contained in this block list.
	 * 
	 * @param entry
	 * @return
	 */
	public boolean contains( String name );
	
	
	/**
	 * This method adds the given entry to the list. Queries for
	 * this element should immediately become effective.
	 * 
	 * @param entry
	 */
	public void add( RBListEntry entry );
	
	public List<RBListEntry> search( String query );
	
	public boolean remove( String name );
	
	public void store();
}
