/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *  
 * This file is part of the jwall-rbld program. jwall-rbld is an implementation 
 * of a simple DNS server for running a local, customized real time block-list. 
 * More information and documentation for the jwall-rbld can be found at
 * 
 *                    http://www.jwall.org/jwall-rbld
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this 
 * program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.rbl.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jwall.util.WildcardMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * This class implements a hash-based RBL.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class RblFile implements RBList {
	/** The global logger for this class */
	static Logger log = LoggerFactory.getLogger(RblFile.class);

	File source;
	Map<String, RBListEntry> entries = new HashMap<String, RBListEntry>();

	Integer maxId = 1;

	/**
	 * 
	 * @param file
	 * @throws IOException
	 */
	public RblFile(File file) throws IOException {
		source = file;
		if (source == null)
			source = new File(File.separator + "opt" + File.separator
					+ "modsecurity" + File.separator + "var" + File.separator
					+ "jwall.rbl");

		log.debug("RBL file is {}", source);
		readDatabase(source);
	}

	/**
	 * This method parses the given file and reads all entries found within. The
	 * file is expected to be in CSV format
	 * 
	 * @param file
	 * @throws IOException
	 */
	protected void readDatabase(File file) throws IOException {

		log.debug("Reading existing RBL list from {}", file);

		if (!file.isFile()) {
			log.debug("RblFile {} does not exist, cannot read stored entries.",
					file);
			return;
		}

		BufferedReader r = new BufferedReader(new FileReader(file));
		String line = r.readLine();
		while (line != null) {
			String l = line.trim();

			if (!l.startsWith("#")) {
				log.debug("Found RBL entry: {}", l);
				try {
					String[] tok = line.split(";");
					RBListEntry entry = new RBListEntry(new Integer(tok[0]),
							tok[1]);
					maxId = Math.max(maxId, entry.getId());
					entry.setName(tok[2]);
					entry.setCreated(new Long(tok[3]));

					if (tok.length > 4)
						entry.setLifetime(new Integer(tok[4]));

					if (tok.length > 5)
						entry.setComment(tok[5]);

					entries.put(entry.getKey(), entry);
				} catch (Exception e) {
					log.error("Failed to parse RBL entry: {}", e.getMessage());
					if (log.isDebugEnabled())
						e.printStackTrace();
				}
			} else {
				log.debug("Skipping comment line '{}'", l);
			}
			line = r.readLine();
		}

		r.close();
		log.debug("{} RBL entries loaded.", entries.size());
	}

	/**
	 * Writes this RBL to the given output stream (CSV format).
	 * 
	 * @param o
	 * @throws IOException
	 */
	public void store(OutputStream o) throws IOException {
		log.debug("Storing RBL list to CSV file format...");
		PrintStream out = new PrintStream(o);
		int i = 0;

		for (RBListEntry e : entries.values()) {
			out.print(e.getId()); // 0
			out.print(";");
			out.print(e.getKey()); // 1
			out.print(";");
			out.print(e.getName()); // 2
			out.print(";");
			out.print(e.getCreated()); // 3
			out.print(";");
			out.print(e.getLifetime()); // 4
			out.print(";");
			out.print(e.getComment()); // 5
			out.println();
			i++;
		}

		out.flush();
		out.close();
		log.debug("{} entries written.", i);
	}

	/**
	 * @see org.jwall.rbl.data.RBList#add(org.jwall.rbl.data.RBListEntry)
	 */
	@Override
	public void add(RBListEntry entry) {
		log.debug("Adding object with key '{}'", entry.getKey());
		if (entry.getId() == null)
			entry.setId(++maxId);
		entries.put(entry.getKey(), entry);
	}

	/**
	 * @see org.jwall.rbl.data.RBList#contains(java.lang.String)
	 */
	@Override
	public boolean contains(String name) {
		return entries.containsKey(name);
	}

	/**
	 * @see org.jwall.rbl.data.RBList#lookup(java.lang.String)
	 */
	@Override
	public RBListEntry lookup(String name) {
		log.debug("Looking up object by key '{}'", name);
		return entries.get(name);
	}

	@Override
	public boolean remove(String name) {
		return entries.remove(name) != null;
	}

	public List<RBListEntry> search(String query) {
		List<RBListEntry> results = new ArrayList<RBListEntry>();
		for (String key : entries.keySet()) {
			if (WildcardMatcher.matches(query, key))
				results.add(entries.get(key));
		}
		return results;
	}

	@Override
	public void store() {

		if (source == null)
			return;

		try {
			if (!source.getParentFile().isDirectory())
				source.getParentFile().mkdirs();
		} catch (Exception e) {
			log.error("Failed to create storage directory for RBL: "
					+ e.getMessage());
		}
		try {

			log.info("Storing rbl data in {}",
					source.getAbsolutePath());
			store(new FileOutputStream(source));
		} catch (Exception e) {
			log.error("Failed to store block-list: {}", e.getMessage());
			if (log.isDebugEnabled())
				e.printStackTrace();
		}
	}
}
