/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *  
 * This file is part of the jwall-rbld program. jwall-rbld is an implementation 
 * of a simple DNS server for running a local, customized real time block-list. 
 * More information and documentation for the jwall-rbld can be found at
 * 
 *                    http://www.jwall.org/jwall-rbld
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this 
 * program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.rbl.data;

import java.io.Serializable;

public class RBListEntry implements Serializable {

	/** The unique class ID */
	private static final long serialVersionUID = 5176769620544767453L;

	Integer id;
	String key = "";
	String name = "";
	Long created = System.currentTimeMillis();
	Integer lifetime = 60;
	String comment = "";

	public RBListEntry(Integer id, String key) {
		this.id = id;
		this.key = key;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	public Long getExpiresAt() {
		return created + 1000 * lifetime.longValue();
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setLifetime(Integer seconds) {
		lifetime = seconds;
	}

	public Integer getLifetime() {
		return lifetime;
	}

	public boolean isExpired() {
		return getExpiresAt() <= System.currentTimeMillis();
	}

	public Integer getRemainingLifetime() {
		return (int) ((getExpiresAt() - System.currentTimeMillis()) / 1000);
	}

	public int hashCode() {
		return key.hashCode();
	}
}
