/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *  
 * This file is part of the jwall-rbld program. jwall-rbld is an implementation 
 * of a simple DNS server for running a local, customized real time block-list. 
 * More information and documentation for the jwall-rbld can be found at
 * 
 *                    http://www.jwall.org/jwall-rbld
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this 
 * program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.rbl;

import java.io.File;

import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.jwall.rbl.data.RblSettings;


/**
 * <p>
 * This file implements the RblDaemon, based on the commons-daemon package. This allows
 * the <i>jwall-rbld</i> to be run as a Java daemon using the jsvc package.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class RblDaemon implements Daemon {
	
	/** The server instance, guarded by this daemon */
	RblServer server;

	
	/**
	 * @see org.apache.commons.daemon.Daemon#destroy()
	 */
	@Override
	public void destroy() {
		server = null;
	}

	
	/**
	 * @see org.apache.commons.daemon.Daemon#init(org.apache.commons.daemon.DaemonContext)
	 */
	@Override
	public void init(DaemonContext c) throws Exception {

		String base = System.getenv( RblServer.RBL_HOME );
		if( base == null )
			base = "";
		
		String config = base + File.separator + "etc" + File.separator + "jwall-rbld.conf" ;
		
		RblSettings p = RblSettings.read( config );
		server = new RblServer( p );
	}

	
	/**
	 * @see org.apache.commons.daemon.Daemon#start()
	 */
	@Override
	public void start() throws Exception {
		server.start();
	}

	
	/**
	 * @see org.apache.commons.daemon.Daemon#stop()
	 */
	@Override
	public void stop() throws Exception {
		server.shutdown();
	}
}
