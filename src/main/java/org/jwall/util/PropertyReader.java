package org.jwall.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertyReader {

	static Logger log = LoggerFactory.getLogger(PropertyReader.class);

	public static Properties read(InputStream in) throws Exception {
		Properties p = new Properties();

		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		String line = reader.readLine();
		while (line != null) {

			line = line.trim();
			if (!line.startsWith("#") && !line.isEmpty()) {

				int idx = line.indexOf("=");
				if (idx > 0) {
					String key = line.substring(0, idx);
					String val = line.substring(idx + 1);
					log.debug("Split into   key: '{}'", key);
					log.debug("           value: '{}'", val);
					p.setProperty(key, val);
				}
			} else {
				log.debug("Skipping line: '{}'", line);
			}

			line = reader.readLine();
		}
		reader.close();
		log.debug("Returning properties: {}", p);
		return p;
	}
}
