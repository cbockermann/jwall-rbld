/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *  
 * This file is part of the jwall-rbld program. jwall-rbld is an implementation 
 * of a simple DNS server for running a local, customized real time block-list. 
 * More information and documentation for the jwall-rbld can be found at
 * 
 *                    http://www.jwall.org/jwall-rbld
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this 
 * program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.util;


import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;

/**
 * <p>
 * This class provides some utility functions useful to format elapsed time (usually
 * given in milliseconds).
 * </p>
 * @author Christian Bockermann &lt;chris@jwall.org&g;
 *
 */
public class TimeFormat
{
    /* */
    public final static long SEC_MS = 1000;

    /* Milliseconds of one minute */
    public final static long MIN_MS = 60 * SEC_MS;

    /* Milliseconds of one hour */
    public final static long HOUR_MS = 60 * MIN_MS;

    /* Milliseconds of one day */
    public final static long DAY_MS = 24 * HOUR_MS;

    /* Milliseconds of one year (using 365,5 days) */
    public final static long YEAR_MS = DAY_MS * 365; // + 12 * HOUR_MS;

    /* The number format for formatting the seconds */
    static final NumberFormat fmt = null; //new DecimalFormat( "0.000" );

    static final long[] UNITS = {
        YEAR_MS, DAY_MS, HOUR_MS, MIN_MS
    };

    static final String[] UNIT_NAME = {
        " year", " day", "h", "m"
    };


    public TimeFormat(){

    }


    /**
     * <p>
     * This method takes the given number of milliseconds, <code>time</code>, and creates a
     * new String containing a description of the time by means of days, hours, minutes and
     * seconds. If <code>time</code> is less than any of the mentioned properties, then this
     * field will not be printed, e.g.
     * <ul>
     *   <li>calling <code>format( 1000 )</code> will result in the string <code>&quot;1s&quot;</code> </li>
     *   
     *   <li>calling <code>format( 90000 * 1000 )</code>, i.e. milliseconds of one day + 1 hour, will
     *       result in <code>&quot;1 day 1h&quot;</code>.
     *   </li>
     * </ul>
     * </p>
     * <p>
     * This method is optimized over the old version (<code>formatOld()</code).
     * </p>
     * 
     * @param timeInMilliseconds The time as an amount of milliseconds.
     * @return The time formatted as printable string.
     */
    public String format( long timeInMilliseconds ){

        long ms = timeInMilliseconds;
        long left = ms;
        long units = 0;

        StringBuilder s = new StringBuilder();

        for( int i = 0; i < UNITS.length; i++ ){

            long unit = UNITS[i];
            if( ms > unit ){
                left = timeInMilliseconds % unit;
                units = ( ms - left ) / unit;
                s.append( units );
                s.append( UNIT_NAME[ i ] );
                if( units > 1 && i < 2 )
                    s.append("s");
                s.append( " " );
                ms = left;
            }
        }

        double sec = (( double ) ms) / (double) SEC_MS;
        if( s.length() == 0 || sec > 0 ){
            s.append( sec );
            s.append( "s" );
        }
        return s.toString();
    }


    /**
     * This is the old method for formatting milliseconds. 
     * 
     * @deprecated
     * @param timeInMilliseconds
     * @return
     */
    public String formatOld( long timeInMilliseconds ){

        long ms = timeInMilliseconds;
        long left = ms;
        long units = 0;
        StringBuilder s = new StringBuilder();

        if( ms > YEAR_MS ){
            left = timeInMilliseconds % YEAR_MS;
            units = (ms - left) / YEAR_MS;

            s.append( units );
            if( units > 1 )
                s.append( " years " );
            else
                s.append( " year " );

            ms = left;
        }

        if( ms > DAY_MS ){

            left = timeInMilliseconds % DAY_MS;
            units = (ms - left) / DAY_MS;
            s.append( units );

            if( units > 1 )
                s.append( " days " );
            else
                s.append( " day " );

            ms = left;
        }

        if( ms > HOUR_MS ){

            left = ms % HOUR_MS;
            units = (ms - left) / HOUR_MS;
            s.append( units );
            s.append( "h " );
            ms = left;
        }

        if( ms > MIN_MS ){
            left = ms % MIN_MS;
            units = (ms-left) / MIN_MS;
            s.append( units );
            s.append( "m " );
            ms = left;
        }

        if( ms > 0 ){
            double d = ((double) ms ) / SEC_MS ;
            if( fmt != null )
                s.append( fmt.format( d ) );
            else
                s.append( d );
            s.append( "s" );
        }

        return s.toString();
    }




    public static void main( String[] args ) throws Exception {

        long last = 1000 * 1232736401L;
        System.out.println( "last TS is " + last );
        System.out.println( "cur TS is  " + System.currentTimeMillis() );
        System.out.println("last = " + new Date( last ) );
        long start = System.currentTimeMillis();
        TimeFormat fmt = new TimeFormat();
        fmt.format( start );

        double fmtNewTime = 0.0d;
        double fmtOldTime = 0.0d;
        double cnt = 0.0d;
        int fmtNewQuicker = 0;
        int fmtEquals = 0;
        int fmtquicker = 0;

        for( int i = 0; i < 1000000; i++ ){

            long now = System.currentTimeMillis();
            String s1 = "";
            String s2 = "";
            long fs = System.nanoTime();
            s1 = fmt.format( now );
            long fe = System.nanoTime();
            long fmtNew_took = fe - fs;
            fmtNewTime += (fmtNew_took);

            fs = System.nanoTime();
            s2 = fmt.formatOld( now );
            fe = System.nanoTime();
            long fmtOld_took = fe - fs;
            fmtOldTime += (fmtOld_took);
            cnt += 1.0d;

            if( fmtNew_took < fmtOld_took )
                fmtNewQuicker++;
            else {
                if( fmtOld_took == fmtNew_took ) 
                    fmtEquals++;
                else
                    fmtquicker++;
            }

            if( ! s1.equals( s2 ) ){
                System.out.println("Formatted string differed: s1='" + s1 + "', s2='" + s2 + "'" );
                break;
            }

            //System.out.println( "Time formatting took " + (fe-fs) + " ns" );

            //System.out.println( s );
            //Thread.sleep( 10 );
            //System.out.println( "Since start: " + TimeFormat.format( System.currentTimeMillis() - start ) );
            //Thread.sleep( 43 );
        }
        System.out.println( "Typical format: " + fmt.format( System.currentTimeMillis() - start ) );
        System.out.println( "FormattingNew took " + (fmtNewTime / cnt ) + "ns on average." );
        System.out.println( "FormattingOld took " + (fmtOldTime / cnt) + "ns on average." );
        DecimalFormat dfmt = new DecimalFormat("0.00" );

        double pct = (double) fmtquicker;
        pct = pct / cnt;
        System.out.println( "formatNew was quicker in " + fmtquicker  + " attempts (" + dfmt.format( 100 * pct ) + "%)" );

        pct = (double) fmtNewQuicker;
        pct = pct / cnt;
        System.out.println( "formatOld was quicker in " + fmtNewQuicker + " attempts (" + dfmt.format( 100 * pct ) + "%)" );

        System.out.println( "equal times needed in " + fmtEquals + " attempts." );
    }
}
