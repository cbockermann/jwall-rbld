#!/bin/sh -e
#
# /etc/init.d/jwall-rbld -- startup script for jwall-rbld
#
#
### BEGIN INIT INFO
# Provides:          jwall-rbld
# Required-Start:    $local_fs $remote_fs $network
# Required-Stop:     $local_fs $remote_fs $network
# Should-Start:      $named
# Should-Stop:       $named
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start jwall-rbld
# Description:       Start jwall-rbld Simple RBL DNS server.
### END INIT INFO

# Configuration files
#
# /etc/jwall-rbld.conf
#   This is the main configuration file. Since the jwall-rbld does not
#   require much configuration this file is pretty sparse.
#
#
# Configuration variables (to define in /etc/default/jwall-rbld)
#
# JAVA_HOME  
#   Home of Java installation. 
#
# JAVA_OPTIONS
#   Extra options to pass to the JVM
#
# RBL_HOME
#   The base directory of the jwall-rbl installation. This most likely
#   points to /opt/modsecurity and defines the base directory from which
#   jwall-rbld does check for its configuration.
#
# RBL_USER
#   if set, then used as a username to run the server as

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
NAME=jwall-rbld
DESC="jwall-rbl server"
RBL_HOME=/opt/modsecurity
LOGDIR="/var/log/jwall-rbld"
JWALL_TOOLS_JAR="$RBL_HOME/lib/jwall-rbld.jar"
DEFAULT=/etc/default/$NAME
DAEMON=/usr/bin/jsvc
export RBL_HOME

if [ `id -u` -ne 0 ]; then
	echo "You need root privileges to run this script"
	exit 1
fi


. /lib/lsb/init-functions

if [ -r /etc/default/rcS ]; then
	. /etc/default/rcS
fi


# Run jwall-rbld as this user ID (default: jwall)
# Set this to an empty string to prevent jwall-rbld from starting automatically
RBL_USER=jwall
            
# Extra options to pass to the JVM         
# Set java.awt.headless=true if JAVA_OPTIONS is not set so the
# Xalan XSL transformer can work without X11 display on JDK 1.4+
# It also sets the maximum heap size to 256M to deal with most cases.
JAVA_OPTIONS="-Xmx256m -Djava.awt.headless=true"
                                           
# The first existing directory is used for JAVA_HOME (if JAVA_HOME is not
# defined in /etc/default/jwall-rbld). Should contain a list of space separated directories.
JDK_DIRS="
	  /usr/lib/jvm/default-java \
	  /usr/lib/jvm/java-6-sun \
	  /usr/lib/jvm/java-6-openjdk \
	  /usr/lib/jvm/java-1.5.0-sun \
	  /usr/lib/jvm/java-gcj \
	  /usr/lib/j2sdk1.6-sun \
	  /usr/lib/j2sdk1.5-sun \
	  /usr/lib/j2sdk1.5-ibm \
	  /usr/lib/j2sdk1.4-sun \
	  /usr/lib/j2sdk1.4 \
	  /usr/lib/j2se/1.4 \
	  /usr/lib/kaffe/ \
	 "

# overwrite settings from default file
if [ -f "$DEFAULT" ]; then
	. "$DEFAULT"
fi

[ -f "$DAEMON" ] || exit 0


# Look for the right JVM to use
for jdir in $JDK_DIRS; do
	if [ -d "$jdir" -a -z "${JAVA_HOME}" ]; then
		JAVA_HOME="$jdir"
	fi
done
export JAVA_HOME
export JAVA="$JAVA_HOME/bin/java"

JAVA_OPTIONS="$JAVA_OPTIONS  \
  -Djava.library.path=/usr/lib \
  -Drbl.port=$RBL_PORT -Drbl.address=$RBL_ADDRESS \
  -Drbl.permission.file=$RBL_PERMISSION_FILE \
  -Drbl.admin.port=$RBL_ADMIN_PORT -Drbl.file=$RBL_FILE"


export JAVA_OPTIONS
                                                                                
# Define other required variables
PIDFILE="/var/run/$NAME.pid"
JSVC_CLASSPATH="/usr/share/java/commons-daemon.jar:$JWALL_TOOLS_JAR:$JAVA_HOME/lib/tools.jar"
ROTATELOGS=/usr/sbin/rotatelogs
HOSTNAME=$(uname -n)
RBL_SHUTDOWN=10

##################################################
# Check for JAVA_HOME
##################################################
if [ -z "$JAVA_HOME" ]; then
	log_failure_msg "Could not start $DESC because no Java Development Kit"
	log_failure_msg "(JDK) was found. Please download and install JDK 1.4 or higher and set"
	log_failure_msg "JAVA_HOME in /etc/default/jetty to the JDK's installation directory."
	exit 0
fi


##################################################
# Do the action
##################################################
case "$1" in
  start)
	log_daemon_msg "Starting $DESC." "$NAME"
	if start-stop-daemon --quiet --test --start --pidfile "$PIDFILE" \
	                --user "$RBL_USER" --startas "$JAVA" > /dev/null; then 

		if [ -f $PIDFILE ] ; then
			log_warning_msg "$PIDFILE exists, but jwall-rbl was not running. Ignoring $PIDFILE"
		fi

		if [ ! -d $LOGDIR ]; then
			mkdir -p $LOGDIR
		fi
		chown -R $RBL_USER:adm "$LOGDIR"

		if [ -s "$LOGDIR/out.log" ]; then
			log_progress_msg "Rotate logs"
			$ROTATELOGS "$LOGDIR/out.log" 86400 \
				< "$LOGDIR/out.log" || true
		fi
		> "$LOGDIR/out.log"
		chown -R $RBL_USER:adm "$LOGDIR"

		$DAEMON -user "$RBL_USER" -cp "$JSVC_CLASSPATH" \
		    -outfile $LOGDIR/out.log -errfile $LOGDIR/out.log \
		    -pidfile "$PIDFILE" $JAVA_OPTIONS org.jwall.rbl.RblDaemon \
		    $JETTY_ARGS $CONFIGS

		log_daemon_msg "$DESC started." "$NAME"

		sleep 5
		if start-stop-daemon --test --start --pidfile "$PIDFILE" \
			--user $RBL_USER --startas "$JAVA" >/dev/null; then
			log_end_msg 1
		else
			log_end_msg 0
		fi

	else
		log_warning_msg "(already running)."
		log_end_msg 0
		exit 1
	fi
	;;

  stop)
	log_daemon_msg "Stopping $DESC." "$NAME"

	if start-stop-daemon --quiet --test --start --pidfile "$PIDFILE" \
		--user "$RBL_USER" --startas "$JAVA" > /dev/null; then
		if [ -x "$PIDFILE" ]; then
			log_warning_msg "(not running but $PIDFILE exists)."
		else
			log_warning_msg "(not running)."
		fi
	else
		start-stop-daemon --quiet --stop \
			--pidfile "$PIDFILE" --user "$RBL_USER" \
			--startas "$JAVA" > /dev/null
		while ! start-stop-daemon --quiet --test --start \
			  --pidfile "$PIDFILE" --user "$RBL_USER" \
			  --startas "$JAVA" > /dev/null; do 
			sleep 1
			log_progress_msg "."
			RBL_SHUTDOWN=`expr $RBL_SHUTDOWN - 1` || true
			if [ $RBL_SHUTDOWN -ge 0 ]; then
				start-stop-daemon --oknodo --quiet --stop \
					--pidfile "$PIDFILE" --user "$RBL_USER" \
					--startas "$JAVA"
			else
				log_progress_msg " (killing) "
				start-stop-daemon --stop --signal 9 --oknodo \
					--quiet --pidfile "$PIDFILE" \
					--user "$RBL_USER"
			fi
		done
		rm -f "$PIDFILE"
		log_daemon_msg "$DESC stopped." "$NAME"
		log_end_msg 0
	fi
	;;

  status)
	if start-stop-daemon --quiet --test --start --pidfile "$PIDFILE" \
		--user "$RBL_USER" --startas "$JAVA" > /dev/null; then

		if [ -f "$PIDFILE" ]; then
		    log_success_msg "$DESC is not running, but pid file exists."
			exit 1
		else
		    log_success_msg "$DESC is not running."
			exit 3
		fi
	else
		log_success_msg "$DESC is running with pid `cat $PIDFILE`."
	fi
	;;

  restart|force-reload)
	if ! start-stop-daemon --quiet --test --start --pidfile "$PIDFILE" \
		--user "$RBL_USER" --startas "$JAVA" > /dev/null; then
		$0 stop $*
		sleep 1
	fi
	$0 start $*
	;;

  try-restart)
	if start-stop-daemon --quiet --test --start --pidfile "$PIDFILE" \
		--user "$RBL_USER" --startas "$JAVA" > /dev/null; then
		$0 start $*
	fi
	;;

  check)
	log_success_msg "Checking arguments for jwall-rbld: "
	log_success_msg ""
	log_success_msg "PIDFILE        =  $PIDFILE"
	log_success_msg "JAVA_OPTIONS   =  $JAVA_OPTIONS"
	log_success_msg "JAVA           =  $JAVA"
	log_success_msg "RBL_USER       =  $RBL_USER"
	log_success_msg "ARGUMENTS      =  $ARGUMENTS"
        
	if [ -f $PIDFILE ]
	then
		log_success_msg "$DESC is running with pid `cat $PIDFILE`."
		exit 0
	fi
	exit 1
	;;

  *)
	log_success_msg "Usage: $0 {start|stop|restart|force-reload|try-restart|status|check}"
	exit 1
	;;
esac

exit 0
