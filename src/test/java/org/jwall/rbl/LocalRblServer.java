package org.jwall.rbl;

import org.junit.Test;

public class LocalRblServer {

	/**
	 * A dummy test method - we simply need this class for starting
	 * the RBL server for debugging.
	 */
	@Test
	public void test(){
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		RblServer.main( args );
	}
}