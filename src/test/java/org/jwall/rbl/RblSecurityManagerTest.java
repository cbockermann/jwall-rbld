package org.jwall.rbl;

import java.net.InetAddress;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;
import org.jwall.rbl.dns.RblSecurityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RblSecurityManagerTest {

	static Logger log = LoggerFactory.getLogger(RblSecurityManagerTest.class);

	@Test
	public void test() {

		try {
			RblSecurityManager sec = RblSecurityManager.getInstance();

			URL url = RblSecurityManagerTest.class
					.getResource("/test.permissions");
			log.info("Reading permissions from {}", url);
			sec.readPermissions(url.openStream());

			InetAddress addr = InetAddress.getByName("::1");
			log.info("Checking permissions for address '{}'", addr);
			Assert.assertTrue(sec.hasPermission(addr,
					RblSecurityManager.BLOCK_PERMISSION));

			addr = InetAddress.getByName("127.0.0.1");
			log.info("Checking permissions for address '{}'", addr);
			Assert.assertTrue(sec.hasPermission(addr,
					RblSecurityManager.BLOCK_PERMISSION));

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Error: " + e.getMessage());
		}
	}
}
