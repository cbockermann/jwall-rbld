package org.jwall.rbl;

import static org.junit.Assert.fail;

import java.io.File;
import java.net.URL;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.jwall.rbl.data.RBListEntry;
import org.jwall.rbl.data.RblFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RblFileTest {

	static Logger log = LoggerFactory.getLogger(RblFileTest.class);

	@Test
	public void testRblFile() {

		URL url = RblFileTest.class.getResource("/rbl.txt");
		log.info("Reading rbl list from {}", url);
		File file = new File(url.getFile());
		log.info("  file is {}", file);

		try {
			RblFile rbl = new RblFile(file);

			List<RBListEntry> list = rbl.search("*");
			for (RBListEntry entry : list) {
				log.info("{}", entry);
				log.info("  Entry for {}", entry.getName());
				log.info("  Created at {}", new Date(entry.getCreated()));
				log.info("  Expires in {}", new Date(entry.getExpiresAt()));
				log.info("  Lifetime in {} seconds", entry.getLifetime());
			}

		} catch (Exception e) {
			fail("Error creating RBL from file: " + e.getMessage());
		}
	}
}
