#
#  Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
#   
#  This file is part of the jwall-rbld program. jwall-rbld is an implementation 
#  of a simple DNS server for running a local, customized real time block-list. 
#  More information and documentation for the jwall-rbld can be found at
#  
#                     http://www.jwall.org/jwall-rbld
#  
#  This program is free software; you can redistribute it and/or modify it under
#  the terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 3 of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License along with this 
#  program; if not, see <http://www.gnu.org/licenses/>.
#
#
VERSION=0.5.1
REVISION=0
NAME=jwall-rbld
BUILD=$(PWD)/.build_tmp
RPMBUILD=$(PWD)/.build_rpm
DEB_FILE=${NAME}-${VERSION}-$(REVISION).deb
RPM_FILE=${NAME}-${VERSION}-$(REVISION).noarch.rpm
RELEASE_DIR=releases
BASE_DIR=opt/modsecurity
BASE=jwall-rbld
DIST=jwall
ARCH=noarch
MD5=`which md5sum`


all:  assembly rpm deb zip


assembly:
	rm -rf ${RELEASE_DIR}
	mkdir -p ${RELEASE_DIR}
#	mvn assembly:assembly
	mvn package


pre-package:
	echo "Preparing package build in ${RPMBUILD}"
	mkdir -p ${RPMBUILD}
	cp -a dist/etc ${RPMBUILD}/
        

rpm:    pre-package
	mkdir -p ${RELEASE_DIR}
	echo "Using RPMBUILD $(RPMBUILD)"
	rm -rf ${RPMBUILD}
	mkdir -p ${RPMBUILD}
	mkdir -p ${RPMBUILD}/tmp
	mkdir -p ${RPMBUILD}/RPMS
	mkdir -p ${RPMBUILD}/RPMS/${ARCH}
	mkdir -p ${RPMBUILD}/BUILD
	mkdir -p ${RPMBUILD}/SRPMS
	rm -rf ${RPMBUILD}/BUILD
	mkdir -p ${RPMBUILD}/BUILD
	#cp -a dist/opt ${RPMBUILD}/BUILD
	mkdir -p ${RPMBUILD}/SPECS
	cp -a dist/jwall-rbld.spec ${RPMBUILD}/SPECS
	mkdir -p ${RPMBUILD}/BUILD/opt/modsecurity/lib
	cp target/jwall-rbld.jar ${RPMBUILD}/BUILD/${BASE_DIR}/lib/jwall-rbld.jar
	find $(RPMBUILD)/BUILD -type f | sed -e s/^.*\.build_rpm\\/BUILD// | grep -v DEBIAN > ${RPMBUILD}/tmp/rpmfiles.list
	rpmbuild -v --target noarch --sign --define '_topdir ${RPMBUILD}' --define '_version ${VERSION}' --define '_revision ${REVISION}' -bb ${RPMBUILD}/SPECS/jwall-rbld.spec --buildroot ${RPMBUILD}/BUILD
	cp ${RPMBUILD}/RPMS/${ARCH}/${RPM_FILE} ${RELEASE_DIR}
	${MD5} ${RELEASE_DIR}/${RPM_FILE} > ${RELEASE_DIR}/${RPM_FILE}.md5

release-rpm:
	mkdir -p /var/www/download.jwall.org/htdocs/yum/${DIST}/noarch
	cp ${RELEASE_DIR}/${RPM_FILE} /var/www/download.jwall.org/htdocs/yum/${DIST}/noarch/
	createrepo /var/www/download.jwall.org/htdocs/yum/${DIST}/




deb:	assembly
	mkdir -p ${BUILD}/${BASE_DIR}/lib
	mkdir -p ${BUILD}/DEBIAN
	cp dist/DEBIAN/* ${BUILD}/DEBIAN/
	cp -a dist/etc ${BUILD}/ 
	chmod 755 ${BUILD}/DEBIAN/p*
	cd ${BUILD} && find opt -type f -exec ${MD5} {} \; > DEBIAN/md5sums && cd ..
	cd ${BUILD} && find etc -type f -exec ${MD5} {} \; >> DEBIAN/md5sums && cd ..
	cp target/jwall-rbld.jar ${BUILD}/${BASE_DIR}/lib/jwall-rbld.jar
	dpkg -b ${BUILD} ${RELEASE_DIR}/${DEB_FILE}
	rm -rf ${BUILD}
	debsigs --sign=origin --default-key=C5C3953C ${RELEASE_DIR}/${DEB_FILE}


release-deb:
	reprepro --ask-passphrase -b /var/www/download.jwall.org/htdocs/debian includedeb ${DIST} ${RELEASE_DIR}/${DEB_FILE}


unrelease-deb:
	reprepro --ask-passphrase -b /var/www/download.jwall.org/htdocs/debian remove ${DIST} jwall-rbld


zip:	
	BASE=jwall-rbld
	mkdir -p ${BUILD}/zip/${BASE}/etc
	mkdir -p ${BUILD}/zip/${BASE}/bin
	cp src/main/bin/jwall-rbld ${BUILD}/zip/${BASE}/bin/
	cp dist/etc/jwall-* ${BUILD}/zip/${BASE}/etc/
	mkdir -p ${BUILD}/zip/${BASE}/lib
	cp target/jwall-rbld.jar ${BUILD}/zip/${BASE}/lib/jwall-rbld.jar
	cd ${BUILD}/zip && zip -r ../../${RELEASE_DIR}/${NAME}-${VERSION}-${REVISION}.zip .
#	rm -rf ${BUILD}/zip
	

clean:	clean-rpm clean-deb clean-zip
	mvn clean

clean-rpm:
	rm -f ${RPM_FILE}


clean-deb:
	rm -rf ${BUILD}
	rm -f ${DEB_FILE}

clean-zip:
	rm -rf ${BUILD}/zip
	rm -f ${NAME}-${VERSION}.zip

upload:
	cd ${RELEASE_DIR} && scp * chris@jwall.org:/var/www/download.jwall.org/upload/


release: upload
